package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.EnergyDTO;
import ro.tuc.ds2020.dtos.ErrorMessageEnergy;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Energy;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.EnergyRepository;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class WebSocketService {
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    @Autowired
    private DeviceRepository deviceRepository;
    @Autowired
    private EnergyRepository energyRepository;


    public void sendMessage(UUID deviceId, EnergyDTO energyDTO){
        Device device = deviceRepository.getOne(deviceId);
        if(checkPeak(device)) {
            ErrorMessageEnergy errorMessageEnergy = new ErrorMessageEnergy();
            errorMessageEnergy.setMessage("it is an error at device " + device.getId()+" with sensor description "
                    +device.getSensorDescription()+" exceeding Maximum peak of "+device.getMax()
                    +". Alert for measurement "+energyDTO.getId()+" with value "+energyDTO.getKw());
            simpMessagingTemplate.convertAndSend("/topic/" + device.getUser().getUsername() + "/errenergy"
                    , errorMessageEnergy);
        }
    }

    private boolean checkPeak(Device device){
        List<Energy> energyList = energyRepository.findAllByDevice_Id(device.getId());
        List<Energy> energyListSorted = energyList.stream().sorted(Comparator.comparing(Energy::getTimestamp).reversed())
                .collect(Collectors.toList());
//        int i =0;
//        for (Energy e:energyListSorted) {
//            System.out.println(i+" ---------- "+e.getKw()+" ------------- "+e.getTimestamp());
//            i++;
//        }
        double peak = (60 * 60 * 1000.0)*(energyListSorted.get(0).getKw() - energyListSorted.get(1).getKw())/(energyListSorted.get(0).getTimestamp().getTime() - energyListSorted.get(1).getTimestamp().getTime());
        //System.out.println("PEEEEEEEEEEEEEEEAK="+peak);
        return peak > device.getSensorMax();
    }
}
