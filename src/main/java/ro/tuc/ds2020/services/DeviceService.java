package ro.tuc.ds2020.services;

import ro.tuc.ds2020.dtos.DeviceDTO;

import java.util.List;
import java.util.UUID;

public interface DeviceService {
    DeviceDTO create(DeviceDTO deviceDTO);
    DeviceDTO retrieve(UUID id);
    List<DeviceDTO> retrieveAll();
    List<DeviceDTO> retrieveByUserId(UUID userId);
    List<DeviceDTO> retrieveMyDevices(String username);
    DeviceDTO update(UUID id, DeviceDTO deviceDTO);
    void delete(UUID id);
}
