package ro.tuc.ds2020.services;

import ro.tuc.ds2020.dtos.UserSignInDTO;
import ro.tuc.ds2020.entities.User;

import java.util.UUID;

public interface UserService {
    void delete(UUID id);

    User findOne(String username);

    User findById(UUID id);

    User save(UserSignInDTO user);
}
