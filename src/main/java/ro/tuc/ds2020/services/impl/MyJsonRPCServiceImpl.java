package ro.tuc.ds2020.services.impl;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.Energy;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.services.MyJsonRPCService;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AutoJsonRpcServiceImpl
public class MyJsonRPCServiceImpl implements MyJsonRPCService {

    private DeviceRepository deviceRepository;

    @Autowired
    public MyJsonRPCServiceImpl(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Override
    public String echo(String str) {
        return "echo: "+str;
    }

    @Override
    public List<Double> historicalEnergyConsumption(int days, UUID device) {
        List<Energy> energyList = deviceRepository.getOne(device).getEnergy().stream()
                .sorted(Comparator.comparing(Energy::getTimestamp).reversed()).collect(Collectors.toList());
        List<Double> result = new ArrayList<>();
        Timestamp timestamp = new Timestamp(new Date().getTime());
        Instant instant = timestamp.toInstant().truncatedTo(ChronoUnit.HOURS).minus(1, ChronoUnit.HOURS);
        int energyIndex = 0;
        System.out.println(energyList.get(energyIndex).getTimestamp());
        for (int i = 0; i < days*24; i++) {
            double avg = 0.0;
            int count = 0;
            while(instant.equals(energyList.get(energyIndex).getTimestamp().toInstant().truncatedTo(ChronoUnit.HOURS))){
                avg+=energyList.get(energyIndex).getKw();
                energyIndex++;
                count++;
            }
            if(count == 0){
                result.add(0.0);
            }else {
                result.add(avg / count);
            }
            instant = instant.minus(1, ChronoUnit.HOURS);
        }
        return result;
    }

    @Override
    public List<Double> averagedEnergyConsumption(UUID device) {
        List<Double> historyList = historicalEnergyConsumption(7,device);
        return simpleMovingAverage(historyList, 24);
    }

    @Override
    public Integer startProgram(int duration, UUID device) {
        System.out.println("enter");
        List<Double> baseline = averagedEnergyConsumption(device);
        double min = Double.MAX_VALUE;
        int minIndex = 0;
        for (int i = 0; i < baseline.size() -duration; i++) {
            double sum = baseline.subList(i, i+duration).stream().reduce(0.0, Double::sum);
            System.out.println(i+" index sum "+sum);
            if(min > sum){
                min = sum;
                minIndex = i;
            }
        }
        System.out.println("minindex "+minIndex);
        return minIndex;
        /*return baseline.indexOf(IntStream.range(0,baseline.size()-duration).mapToObj(i -> {
            return baseline.subList(i,i+duration).stream().mapToDouble(a->a).sum();
        }).min(Double::compare).get());*/
    }

    @Override
    public List<Double> newBaseline(int duration, UUID device) {
        List<Double> baseline = averagedEnergyConsumption(device);
        int start = startProgram(duration, device);
        int deviceMax = deviceRepository.getOne(device).getSensorMax();
        for (int i = 0; i < baseline.size(); i++) {
            if(i>=start && i<start+duration){
                Double value = baseline.get(i);
                value+= deviceMax;
                baseline.set(i,value);
            }
        }
        return baseline;
    }

    private List<Double> simpleMovingAverage(List<Double> in, int k){
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < k; i++) {
            double avg = 0;
            for (int j = 0; j < in.size()/k; j++) {
                avg+=in.get(j*k+i);
            }
            avg /= ((double) in.size() /k);
            result.add(avg);
        }
        return result;
    }


}
