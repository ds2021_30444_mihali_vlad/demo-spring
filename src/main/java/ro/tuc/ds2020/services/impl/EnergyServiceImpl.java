package ro.tuc.ds2020.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.DateDTO;
import ro.tuc.ds2020.dtos.EnergyDTO;
import ro.tuc.ds2020.dtos.builders.EnergyBuilder;
import ro.tuc.ds2020.entities.Energy;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.EnergyRepository;
import ro.tuc.ds2020.services.EnergyService;

import javax.transaction.Transactional;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class EnergyServiceImpl implements EnergyService {
    private EnergyRepository energyRepository;
    private DeviceRepository deviceRepository;

    @Autowired
    public EnergyServiceImpl(EnergyRepository energyRepository, DeviceRepository deviceRepository) {
        this.energyRepository = energyRepository;
        this.deviceRepository = deviceRepository;
    }

    @Override
    public EnergyDTO create(EnergyDTO energyDTO) {
        Energy energy = EnergyBuilder.toEntity(energyDTO);
        if(energyDTO.getDevice() != null){
            energy.setDevice(deviceRepository.getOne(energyDTO.getDevice()));
        }
        return EnergyBuilder.toEnergyDTO(energyRepository.save(energy));
    }

    @Override
    public EnergyDTO retrieve(UUID id) {
        return EnergyBuilder.toEnergyDTO(energyRepository.getOne(id));
    }

    @Override
    public List<EnergyDTO> retrieveAll() {
        return EnergyBuilder.toEnergyDTOList(energyRepository.findAll());
    }

    @Override
    public List<EnergyDTO> retrieveByDeviceId(UUID deviceId) {
        return EnergyBuilder.toEnergyDTOList(energyRepository.findAllByDevice_Id(deviceId));
    }

    @Override
    @Transactional
    public EnergyDTO update(UUID id, EnergyDTO energyDTO) {
        Energy energy = energyRepository.getOne(id);
        if(energyDTO.getKw() != null) {
            energy.setKw(energyDTO.getKw());
        }
        if(energyDTO.getTimestamp()!=null) {
            energy.setTimestamp(energyDTO.getTimestamp());
        }
        if(energyDTO.getDevice() != null){
            energy.setDevice(deviceRepository.getOne(energyDTO.getDevice()));
        }
        return EnergyBuilder.toEnergyDTO(energy);
    }

    @Override
    public void delete(UUID id) {
        energyRepository.deleteById(id);
    }

    @Override
    public List<EnergyDTO> retrieveByMyDeviceId(UUID id, String username) {
        User user = deviceRepository.getOne(id).getUser();
        if(user!=null && user.getUsername().equals(username)) {
            return EnergyBuilder.toEnergyDTOList(energyRepository.findAllByDevice_Id(id));
        }
        else{
            throw new RuntimeException("This is not your device");
        }
    }

    @Override
    public List<EnergyDTO> retrieveByMyDeviceIdAndDate(UUID id, String username, DateDTO dateDTO) {
        User user = deviceRepository.getOne(id).getUser();
        if(user!=null && user.getUsername().equals(username)) {
            return EnergyBuilder.toEnergyDTOList(
                    energyRepository.findAllByDevice_Id(id)
                            .stream()
                            .filter(energy ->
                                    energy.getTimestamp()
                                            .toInstant()
                                            .truncatedTo(ChronoUnit.DAYS)
                                            .equals(dateDTO.getDate().toInstant().truncatedTo(ChronoUnit.DAYS)))
                            .sorted(Comparator.comparing(Energy::getTimestamp))
                            .collect(Collectors.toList()));
        }
        else{
            throw new RuntimeException("This is not your device");
        }
    }
}
