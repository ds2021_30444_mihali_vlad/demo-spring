package ro.tuc.ds2020.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.UserRepository;
import ro.tuc.ds2020.services.DeviceService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
@Service
public class DeviceServiceImpl implements DeviceService {
    private DeviceRepository deviceRepository;
    private UserRepository userRepository;

    @Autowired
    public DeviceServiceImpl(DeviceRepository deviceRepository, UserRepository userRepository) {
        this.deviceRepository = deviceRepository;
        this.userRepository = userRepository;
    }


    @Override
    public DeviceDTO create(DeviceDTO deviceDTO) {
        Device device = DeviceBuilder.toEntity(deviceDTO);
        if(deviceDTO.getUser() != null){
            device.setUser(userRepository.getOne(deviceDTO.getUser()));
        }
        return DeviceBuilder.toDeviceDTO(deviceRepository.save(device));
    }

    @Override
    public DeviceDTO retrieve(UUID id) {
        return DeviceBuilder.toDeviceDTO(deviceRepository.getOne(id));
    }

    @Override
    public List<DeviceDTO> retrieveAll() {
        return DeviceBuilder.toDeviceDTOList(deviceRepository.findAll());
    }

    @Override
    public List<DeviceDTO> retrieveByUserId(UUID userId) {
        return DeviceBuilder.toDeviceDTOList(deviceRepository.findDevicesByUser_Id(userId));
    }

    @Override
    public List<DeviceDTO> retrieveMyDevices(String username) {
        return DeviceBuilder.toDeviceDTOList(deviceRepository.findDevicesByUser_Username(username));
    }

    @Override
    @Transactional
    public DeviceDTO update(UUID id, DeviceDTO deviceDTO) {
        Device device = deviceRepository.getOne(id);
        if(deviceDTO.getDescription()!=null) {
            device.setDescription(deviceDTO.getDescription());
        }
        if(deviceDTO.getLocation()!=null) {
            device.setLocation(deviceDTO.getLocation());
        }
        if(deviceDTO.getMax()!=null) {
            device.setMax(deviceDTO.getMax());
        }
        if(deviceDTO.getAverage()!=null) {
            device.setAverage(deviceDTO.getAverage());
        }
        if(deviceDTO.getSensorDescription()!=null) {
            device.setSensorDescription(deviceDTO.getSensorDescription());
        }
        if(deviceDTO.getSensorMax()!=null) {
            device.setSensorMax(deviceDTO.getSensorMax());
        }
        return DeviceBuilder.toDeviceDTO(device);
    }

    @Override
    public void delete(UUID id) {
        deviceRepository.deleteById(id);
    }
}
