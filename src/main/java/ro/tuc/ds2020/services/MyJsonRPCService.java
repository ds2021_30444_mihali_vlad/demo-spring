package ro.tuc.ds2020.services;

import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;

import java.util.List;
import java.util.UUID;

@JsonRpcService("/jsonrpc4j/api")
public interface MyJsonRPCService {
    String echo(@JsonRpcParam("str") String str);

    List<Double> historicalEnergyConsumption(@JsonRpcParam("days") int days, @JsonRpcParam("device") UUID device);

    List<Double> averagedEnergyConsumption(@JsonRpcParam("device") UUID device);

    Integer startProgram(@JsonRpcParam("duration") int duration, @JsonRpcParam("device") UUID device);

    List<Double> newBaseline(@JsonRpcParam("duration") int duration, @JsonRpcParam("device") UUID device);
}
