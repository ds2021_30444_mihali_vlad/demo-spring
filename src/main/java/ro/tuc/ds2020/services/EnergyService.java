package ro.tuc.ds2020.services;

import ro.tuc.ds2020.dtos.DateDTO;
import ro.tuc.ds2020.dtos.EnergyDTO;

import java.util.List;
import java.util.UUID;

public interface EnergyService {
    EnergyDTO create(EnergyDTO energyDTO);
    EnergyDTO retrieve(UUID id);
    List<EnergyDTO> retrieveAll();
    List<EnergyDTO> retrieveByDeviceId(UUID deviceId);
    EnergyDTO update(UUID id, EnergyDTO energyDTO);
    void delete(UUID id);
    List<EnergyDTO> retrieveByMyDeviceId(UUID id, String username);
    List<EnergyDTO> retrieveByMyDeviceIdAndDate(UUID id, String username, DateDTO dateDTO);
}
