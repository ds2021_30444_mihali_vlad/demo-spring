package ro.tuc.ds2020.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Constants {
    @Value("${token.ACCESS_TOKEN_VALIDITY_SECONDS}")
    public long ACCESS_TOKEN_VALIDITY_SECONDS;
    @Value("${token.SIGNING_KEY}")
    public String SIGNING_KEY;
    @Value("${token.AUTHORITIES_KEY}")
    public String AUTHORITIES_KEY;
    @Value("${token.TOKEN_PREFIX}")
    public String TOKEN_PREFIX;
    @Value("${token.HEADER_STRING}")
    public String HEADER_STRING;
}

