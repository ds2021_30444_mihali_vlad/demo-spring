package ro.tuc.ds2020.dtos;

import lombok.Data;

@Data
public class ErrorMessageEnergy {
    String message;
}
