package ro.tuc.ds2020.dtos;

import lombok.Data;

import java.sql.Timestamp;
import java.util.UUID;

@Data
public class EnergyDTO {
    private UUID id;
    private Integer kw;
    private Timestamp timestamp;
    private UUID device;
}
