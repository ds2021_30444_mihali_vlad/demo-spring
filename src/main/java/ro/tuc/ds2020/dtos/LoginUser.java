package ro.tuc.ds2020.dtos;

import lombok.Data;

@Data
public class LoginUser {
    private String username;
    private String password;
}
