package ro.tuc.ds2020.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class DateDTO {
    private Date date;
}
