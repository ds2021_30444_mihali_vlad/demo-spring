package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.UserSignInDTO;
import ro.tuc.ds2020.entities.Role;
import ro.tuc.ds2020.entities.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserBuilder {

    private UserBuilder() {
    }

    public static UserDTO toUserDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setUsername(user.getUsername());
        userDTO.setName(user.getName());
        userDTO.setAddress(user.getAddress());
        userDTO.setBirthDate(user.getBirthDate());
        return userDTO;
    }

    public static List<UserDTO> toUserDTOList(List<User> users) {
        return users.stream().map(UserBuilder::toUserDTO).collect(Collectors.toList());
    }


    public static User toEntity(UserDTO userDTO) {
        return new User(userDTO.getUsername(),
                userDTO.getName(),
                userDTO.getAddress(),
                userDTO.getBirthDate());
    }

    public static User signIntoEntity(UserSignInDTO userDTO) {
        User user = new User();
        user.setUsername(userDTO.getUsername());
        user.setAddress(userDTO.getAddress());
        user.setBirthDate(userDTO.getBirthDate());
        user.setName(userDTO.getName());
        List<Role> roleList = new ArrayList<>();
        roleList.add(new Role("USER"));
        user.setRoles(roleList);
        return user;
    }
}
