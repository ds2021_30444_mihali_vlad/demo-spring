package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Energy;

import java.util.List;
import java.util.stream.Collectors;

public class DeviceBuilder {
    public static Device toEntity(DeviceDTO deviceDTO){
        Device device = new Device();
        device.setDescription(deviceDTO.getDescription());
        device.setLocation(deviceDTO.getLocation());
        device.setMax(deviceDTO.getMax());
        device.setAverage(deviceDTO.getAverage());
        device.setSensorDescription(deviceDTO.getSensorDescription());
        device.setSensorMax(deviceDTO.getSensorMax());
        return device;
    }

    public static DeviceDTO toDeviceDTO(Device device){
        DeviceDTO deviceDTO = new DeviceDTO();
        deviceDTO.setId(device.getId());
        deviceDTO.setDescription(device.getDescription());
        deviceDTO.setLocation(device.getLocation());
        deviceDTO.setMax(device.getMax());
        deviceDTO.setAverage(device.getAverage());
        deviceDTO.setSensorDescription(device.getSensorDescription());
        deviceDTO.setSensorMax(device.getSensorMax());
        if(device.getUser()!=null) {
            deviceDTO.setUser(device.getUser().getId());
        }
        if(device.getEnergy()!=null && !device.getEnergy().isEmpty()){
            deviceDTO.setEnergy(device.getEnergy().stream().map(Energy::getId).collect(Collectors.toList()));
        }
        return deviceDTO;
    }

    public static List<DeviceDTO> toDeviceDTOList(List<Device> deviceList) {
        return deviceList.stream().map(DeviceBuilder::toDeviceDTO).collect(Collectors.toList());
    }
}
