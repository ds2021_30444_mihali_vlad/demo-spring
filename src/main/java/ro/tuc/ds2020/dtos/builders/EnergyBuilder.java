package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.EnergyDTO;
import ro.tuc.ds2020.entities.Energy;

import java.util.List;
import java.util.stream.Collectors;

public class EnergyBuilder {
    public static Energy toEntity(EnergyDTO energyDTO){
        Energy energy = new Energy();
        energy.setKw(energyDTO.getKw());
        energy.setTimestamp(energyDTO.getTimestamp());
        return energy;
    }

    public static EnergyDTO toEnergyDTO(Energy energy){
        EnergyDTO energyDTO = new EnergyDTO();
        energyDTO.setId(energy.getId());
        energyDTO.setKw(energy.getKw());
        energyDTO.setTimestamp(energy.getTimestamp());
        if(energy.getDevice()!=null) {
            energyDTO.setDevice(energy.getDevice().getId());
        }
        return energyDTO;
    }

    public static List<EnergyDTO> toEnergyDTOList(List<Energy> energyList) {
        return energyList.stream().map(EnergyBuilder::toEnergyDTO).collect(Collectors.toList());
    }
}
