package ro.tuc.ds2020.dtos;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class DeviceDTO {
    private UUID id;
    private String description;
    private String location;
    private Integer max;
    private Integer average;
    private String sensorDescription;
    private Integer sensorMax;
    private UUID user;
    private List<UUID> energy;
}
