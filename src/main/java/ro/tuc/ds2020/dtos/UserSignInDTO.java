package ro.tuc.ds2020.dtos;

import lombok.Data;

import java.sql.Timestamp;
@Data
public class UserSignInDTO {
    private String username;
    private String password;
    private String name;
    private String address;
    private Timestamp birthDate;
}
