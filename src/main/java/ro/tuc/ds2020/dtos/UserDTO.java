package ro.tuc.ds2020.dtos;

import lombok.Data;

import java.sql.Timestamp;
import java.util.UUID;

@Data
public class UserDTO {
    private UUID id;
    private String username;
    private String name;
    private String address;
    private Timestamp birthDate;

}
