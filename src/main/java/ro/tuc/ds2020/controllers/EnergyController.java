package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DateDTO;
import ro.tuc.ds2020.dtos.EnergyDTO;
import ro.tuc.ds2020.services.EnergyService;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/energy")
public class EnergyController {
    private EnergyService energyService;
    @Autowired
    public EnergyController(EnergyService energyService) {
        this.energyService = energyService;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<EnergyDTO> getEnergy(@PathVariable(value = "id") UUID id){
        try{
            EnergyDTO energyDTO = energyService.retrieve(id);
            return new ResponseEntity<>(energyDTO, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/")
    public ResponseEntity<List<EnergyDTO>> getAllEnergy(){
        try{
            List<EnergyDTO> energyDTOList = energyService.retrieveAll();
            return new ResponseEntity<>(energyDTOList, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/device/{id}")
    public ResponseEntity<List<EnergyDTO>> getAllEnergyByDeviceId(@PathVariable(value = "id") UUID id){
        try{
            List<EnergyDTO> energyDTOList = energyService.retrieveByDeviceId(id);
            return new ResponseEntity<>(energyDTOList, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PostMapping("/my/device/{id}")
    public ResponseEntity<List<EnergyDTO>> getAllMyEnergyByDeviceIdAndDate(@PathVariable(value = "id") UUID id, Principal principal, @RequestBody DateDTO dateDTO){
        try{
            List<EnergyDTO> energyDTOList = energyService.retrieveByMyDeviceIdAndDate(id, principal.getName(), dateDTO);
            return new ResponseEntity<>(energyDTOList, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/my/device/{id}")
    public ResponseEntity<List<EnergyDTO>> getAllMyEnergyByDeviceId(@PathVariable(value = "id") UUID id, Principal principal){
        try{
            List<EnergyDTO> energyDTOList = energyService.retrieveByMyDeviceId(id, principal.getName());
            return new ResponseEntity<>(energyDTOList, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(BAD_REQUEST);
        }
    }


    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/")
    public ResponseEntity<EnergyDTO> postEnergy(@RequestBody EnergyDTO energyDTO){
        try{
            EnergyDTO newEnergyDTO = energyService.create(energyDTO);
            return new ResponseEntity<>(newEnergyDTO, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<?> putEnergy(@PathVariable (value = "id") UUID id, @RequestBody EnergyDTO EnergyDTO){
        try{
            EnergyDTO newEnergyDTO = energyService.update(id, EnergyDTO);
            return new ResponseEntity<>(newEnergyDTO, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(ex.getMessage(),BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEnergy(@PathVariable (value = "id") UUID id){
        try{
            energyService.delete(id);
            return new ResponseEntity<>(OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(ex.getMessage(),BAD_REQUEST);
        }
    }


}
