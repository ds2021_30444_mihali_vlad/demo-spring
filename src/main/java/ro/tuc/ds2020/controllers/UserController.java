package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.UserSignInDTO;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.services.impl.UserServiceImpl;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    //@Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value="/user", method = RequestMethod.GET)
    public List<UserDTO> listUser(){
        return userService.findAll();
    }

    //@Secured("ROLE_USER")
    //@PreAuthorize("hasRole('USER')")
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public User getOne(@PathVariable(value = "id") UUID id){
        return userService.findById(id);
    }
    //@PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value="/signup", method = RequestMethod.POST)
    public User create(@RequestBody UserSignInDTO user){
        return userService.save(user);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value="/user/{id}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable(value = "id") UUID id){
        userService.delete(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/user/{id}")
    public ResponseEntity<?> putUser(@PathVariable (value = "id") UUID id, @RequestBody UserDTO userDTO){
        try{
            UserDTO newUserDTO = userService.update(id, userDTO);
            return new ResponseEntity<>(newUserDTO, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(ex.getMessage(),BAD_REQUEST);
        }
    }
}