package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.services.DeviceService;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/device")
public class DeviceController {
    private DeviceService deviceService;
    @Autowired
    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<DeviceDTO> getDevice(@PathVariable(value = "id") UUID id){
        try{
            DeviceDTO deviceDTO = deviceService.retrieve(id);
            return new ResponseEntity<>(deviceDTO, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/")
    public ResponseEntity<List<DeviceDTO>> getAllDevices(){
        try{
            List<DeviceDTO> deviceDTOList = deviceService.retrieveAll();
            return new ResponseEntity<>(deviceDTOList, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/my")
    public ResponseEntity<List<DeviceDTO>> getMyDevices(Principal principal){
        try{
            List<DeviceDTO> devices = deviceService.retrieveMyDevices(principal.getName());
            return new ResponseEntity<>(devices, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/user/{id}")
    public ResponseEntity<List<DeviceDTO>> getAllDevicesByUserId(@PathVariable(value = "id") UUID id){
        try{
            List<DeviceDTO> deviceDTOList = deviceService.retrieveByUserId(id);
            return new ResponseEntity<>(deviceDTOList, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/")
    public ResponseEntity<DeviceDTO> postDevice(@RequestBody DeviceDTO deviceDTO){
        try{
            DeviceDTO newDeviceDTO = deviceService.create(deviceDTO);
            return new ResponseEntity<>(newDeviceDTO, OK);
        }catch (RuntimeException ex){
            System.out.println(ex.toString());
            return new ResponseEntity<>(BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<?> putDevice(@PathVariable (value = "id") UUID id, @RequestBody DeviceDTO DeviceDTO){
        try{
            DeviceDTO newDeviceDTO = deviceService.update(id, DeviceDTO);
            return new ResponseEntity<>(newDeviceDTO, OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(ex.getMessage(),BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteDevice(@PathVariable (value = "id") UUID id){
        try{
            deviceService.delete(id);
            return new ResponseEntity<>(OK);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(ex.getMessage(),BAD_REQUEST);
        }
    }
}
