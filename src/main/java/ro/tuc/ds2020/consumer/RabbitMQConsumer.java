package ro.tuc.ds2020.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.EnergyDTO;
import ro.tuc.ds2020.services.EnergyService;
import ro.tuc.ds2020.services.WebSocketService;

public class RabbitMQConsumer {

    @Autowired
    private EnergyService energyService;
    @Autowired
    private WebSocketService webSocketService;

    @RabbitListener(queues = "${rabbitmq.queuename}")
    public void receive(EnergyDTO energyDTO) {
        try{
            EnergyDTO newEnergyDTO = energyService.create(energyDTO);
            System.out.println(" [x] Received '" + newEnergyDTO + "'");
            webSocketService.sendMessage(newEnergyDTO.getDevice(), newEnergyDTO);

        }catch (RuntimeException ex){
            System.out.println("Something went wrong when receiving a message");
        }
    }
}