package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.dtos.UserSignInDTO;

import java.sql.Timestamp;
import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@SpringBootTest
@ActiveProfiles(profiles = "test")
@AutoConfigureMockMvc
public class UserControllerUnitTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void registrationTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UserSignInDTO userSignInDTO = new UserSignInDTO();
        userSignInDTO.setAddress("strada 1");
        userSignInDTO.setUsername("user1");
        userSignInDTO.setBirthDate(new Timestamp(new Date().getTime()));
        userSignInDTO.setName("user User");
        userSignInDTO.setPassword("user1234");
        mockMvc.perform(post("/signup")
                .content(objectMapper.writeValueAsString(userSignInDTO))
                .contentType("application/json"))
                .andExpect(status().isOk());
    }
}
