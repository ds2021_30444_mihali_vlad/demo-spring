package ro.tuc.ds2020.services;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.ds2020.dtos.EnergyDTO;

import java.util.List;

import static org.junit.Assert.assertEquals;
@SpringBootTest
@ActiveProfiles(profiles = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class EnergyServiceIntegrationTest {
    @Autowired
    EnergyService energyService;

    @Test
    public void testGetCorrect() {
        List<EnergyDTO> personDTOList = energyService.retrieveAll();
        assertEquals("Test Insert Measurement", 1, personDTOList.size());
    }
}
