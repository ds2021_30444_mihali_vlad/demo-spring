FROM maven:3.6.3-jdk-11 AS builder
COPY ./src /root/src
COPY ./pom.xml /root/
COPY ./checkstyle.xml /root/
WORKDIR /root
RUN mvn package
RUN java -Djarmode=layertools -jar /root/target/ds-2020-0.0.1-SNAPSHOT.jar list
RUN java -Djarmode=layertools -jar /root/target/ds-2020-0.0.1-SNAPSHOT.jar extract
RUN ls -l /root

FROM openjdk:11.0.6-jre
ENV TZ=UTC
ENV DB_IP=ec2-54-155-92-75.eu-west-1.compute.amazonaws.com
ENV DB_PORT=5432
ENV DB_USER=jizhsqspvvsgby
ENV DB_PASSWORD=0d3382898a67418efc47761f651e41efcd9206ad06ec990a918637cb09cd43b1
ENV DB_DBNAME=dasklecl9sd89b

COPY --from=builder /root/dependencies/ ./
COPY --from=builder /root/snapshot-dependencies/ ./

RUN sleep 10
COPY --from=builder /root/spring-boot-loader/ ./
COPY --from=builder /root/application/ ./
ENTRYPOINT ["java","org.springframework.boot.loader.JarLauncher","-XX:+UseContainerSupport -XX:+UnlockExperimentalVMOptions"]